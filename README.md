# HouseTour

Simple House Tour Game (using free assets) where you will be spawned inside a house, can walk around and interact with some of the house's objects.

Made using Unity 3D 2020.3.16f

## Assets

- https://assetstore.unity.com/packages/3d/environments/urban/devden-archviz-vol-1-scotland-158539
- https://assetstore.unity.com/packages/vfx/particles/fire-explosions/procedural-fire-141496
- https://docs.unity3d.com/Packages/com.unity.ui@1.0/manual/index.html?_ga=2.215627863.1339113860.1636464488-751225767.1630917574
- https://docs.unity3d.com/Packages/com.unity.ui.builder@1.0/manual/index.html?_ga=2.215627863.1339113860.1636464488-751225767.1630917574

## Controls

- Walk: WASD
- Camera View: Mouse
- Interact with Object: E
- Exit to Main Menu: Q

## Available Object Interaction

- Fire on Stove
![](https://i.imgur.com/j15YPsk.png)

- TV
![](https://i.imgur.com/fAmHv4T.png)

- Laptop/Computer
![](https://i.imgur.com/UTc766W.png)

- Doors
![](https://i.imgur.com/vMEbnO7.png)

- Lights
![](https://i.imgur.com/TEMgtna.png)

- Water Faucet
![](https://i.imgur.com/hFMFdMu.png)

- TV Shelf
![](https://i.imgur.com/S16ySfP.png)

- Couch
![](https://i.imgur.com/m2BmIhf.png)
